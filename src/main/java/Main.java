import org.jsoup.*;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantLock;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

class NewsContent {
    Elements newsTitle;
    Elements subNewsText;
}

public class Main {
    static LinkedList<String> links = new LinkedList<String>();
    static LinkedList<String> pages = new LinkedList<String>();
    static LinkedList<NewsContent> content = new LinkedList<NewsContent>();
    static ReentrantLock downloadMutex = new ReentrantLock();       //mutex
    static ReentrantLock parseMutex = new ReentrantLock();         //mutex

    static class LinksDownloader extends Thread {
        public void run() {
            while (true) {
                try {
                    downloadMutex.lock();
                    if (links.isEmpty()) {
                        return;
                    }
                    pages.add(GetPage(links.pop()));
                } catch (Exception e) {
                    System.out.println("error getting link");
                } finally {
                    downloadMutex.unlock();
                }
            }
        }
    }


    static class LinksParser extends Thread {
        public void run() {
            while (true) {
                NewsContent Title = new NewsContent();
                try {
                    parseMutex.lock();
                    if (pages.isEmpty()) {
                        return;
                    }
                    Document document = (Document) Jsoup.parse(pages.pop());
                    Title.newsTitle = document.select("div.welcome>h1");
                    Title.subNewsText = document.select("div.news>ul>li");
                    content.add(Title);
                } catch (Exception e) {
                    System.out.println("error parsing link");
                } finally {
                    parseMutex.unlock();
                }
            }
        }
    }


    public static String GetPage(String url) throws IOException, InterruptedException {
        try {
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(url))
                    .build();
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            /*if (response.statusCode() == 200) {
                System.out.println("\nSuccessfully downloaded!");
            }*/
            return response.body().toString();
        }
        catch (Exception exc) {
            System.out.println("Exception " + exc.toString());
            return "";
        }
    }


    public static void GetLinks(String sitePage) {
        Document doc = (Document) Jsoup.parse(sitePage);
        Elements news = doc.select("div.news_list>a.news_list_item");
        for (Element i:news) {
            links.add("https://www.domod.ru" + i.attr("href"));
            System.out.println("https://www.domod.ru" + i.attr("href"));
        }
        System.out.println("\n");
    }


    public static void main(String[] args) throws IOException, InterruptedException {
        String site = GetPage("https://www.domod.ru/");

        GetLinks(site);

        LinksDownloader downloadThread1 = new LinksDownloader();
        LinksDownloader downloadThread2 = new LinksDownloader();
        LinksParser parseThread = new LinksParser();


        downloadThread2.run();

        downloadThread1.run();
        downloadThread1.join();

        downloadThread2.join();

        parseThread.run();
        parseThread.join();

        for (NewsContent i:content) {
           PrintNews(i.newsTitle, i.subNewsText);
        }
    }

    public static void PrintNews(Elements newsTitle, Elements newsText) {
        System.out.println(newsTitle.text() + "\n" + newsText.text() + "\n");
    }
}
